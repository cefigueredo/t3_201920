package test.data_structures;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.UBERTrip;

public class TestMVCModelo {
	
	public ArregloDinamico<UBERTrip> datos1 = new ArregloDinamico<>(1);
	public ArregloDinamico<UBERTrip> datos2= new ArregloDinamico<>(1);
	public ArregloDinamico<UBERTrip> datos3= new ArregloDinamico<>(1);

	public ArregloDinamico<UBERTrip> datos4= new ArregloDinamico<>(1);
	public ArregloDinamico<UBERTrip> datos5= new ArregloDinamico<>(1);
	public ArregloDinamico<UBERTrip> datos6= new ArregloDinamico<>(1);
	
	public ArregloDinamico<UBERTrip> datos7= new ArregloDinamico<>(1);
	public ArregloDinamico<UBERTrip> datos8= new ArregloDinamico<>(1);
	public ArregloDinamico<UBERTrip> datos9= new ArregloDinamico<>(1);

	public MVCModelo<?> modelo = new MVCModelo();
	@Before
	public void setUp1() throws Exception {
		Random rgen = new Random();
		double d1 = rgen.nextDouble()*23;
		for(int i = 0; i < 500000; i++) {
			UBERTrip viaje = new UBERTrip(i, i, d1, i, i, i, i);
			datos1.agregar(viaje);
			datos2.agregar(viaje);
			datos3.agregar(viaje);
		}

	}
	
	public void setUp2() throws Exception {
		Random rgen = new Random();
		double d1 = rgen.nextDouble()*23;
		for(int i = 500000; i > 0; i--) {
			UBERTrip viaje = new UBERTrip(i, i, d1, i, i, i, i);
			datos4.agregar(viaje);
			datos5.agregar(viaje);
			datos6.agregar(viaje);

		}

	}
	public void setUp3() throws Exception {
		Random rgen = new Random();
		double d1 = rgen.nextDouble()*23;
		double d2 = rgen.nextDouble()*500000;
		for(int i = 500000; i > 0; i--) {
			UBERTrip viaje = new UBERTrip(d2, d2, d1, d2, d2, d2, d2);
			datos7.agregar(viaje);
			datos8.agregar(viaje);
			datos9.agregar(viaje);

		}

	}
	
	
	@Test
	public void testShellAscendente() throws Exception {
		setUp1();
		ArregloDinamico<UBERTrip> aux = datos1;
		modelo.mergeSort(datos1);
		assertEquals(datos1.darElemento(0), aux.darElemento(0));
	}
	@Test
	public void testShellDescendente() throws Exception {
		setUp2();
		ArregloDinamico<UBERTrip> aux = datos4;
		modelo.shellSort(datos4);	
		assertEquals(datos4.darElemento(0), aux.darElemento(aux.darTamano()-1));

	}
	@Test
	public void testShellDesordenado() throws Exception {
		setUp3();
		modelo.shellSort(datos7);
	}
	@Test
	public void testQuickAscendente() throws Exception {
		setUp1();
		ArregloDinamico<UBERTrip> aux = datos2;
		modelo.quickSortStart(datos2);	
		assertEquals(datos2.darElemento(0), aux.darElemento(0));

	}
	@Test
	public void testQuickDescendente() throws Exception {
		setUp2();
		ArregloDinamico<UBERTrip> aux = datos5;
		modelo.quickSortStart(datos5);	
		assertEquals(datos5.darElemento(0), aux.darElemento(aux.darTamano()-1));

	}
	@Test
	public void testQuickDesordenado() throws Exception {
		setUp3();
		modelo.quickSortStart(datos8);	
	}@Test
	public void testMergeAscendente() throws Exception {
		setUp1();
		ArregloDinamico<UBERTrip> aux = datos3;
		modelo.mergeSort(datos3);
		assertEquals(datos3.darElemento(0), aux.darElemento(0));

	}
	@Test
	public void testMergeDescendente() throws Exception {
		setUp2();
		ArregloDinamico<UBERTrip> aux = datos6;
		modelo.mergeSort(datos6);		
		assertEquals(datos5.darElemento(0), aux.darElemento(aux.darTamano()-1));
	}
	@Test
	public void testMergeDesordenado() throws Exception {
		setUp3();
		modelo.mergeSort(datos9);
	}
}
