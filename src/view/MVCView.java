package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Crear Arreglo Dinamico de Strings");
			System.out.println("2. Cargar datos");
			System.out.println("3. Buscar por hora");
			System.out.println("4. Ordenar con Shell Sort");
			System.out.println("5. Ordenar con Merge Sort");
			System.out.println("6. Ordenar con Quick Sort");
			System.out.println("7. Comparar tiempo de ordenamientos");
			System.out.println("8. Imprimir el Arreglo");
			System.out.println("9. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}
}
