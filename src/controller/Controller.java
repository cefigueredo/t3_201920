package controller;

import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.UBERTrip;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	@SuppressWarnings("unchecked")
	public void run() throws Exception 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		UBERTrip viaje;
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			
			switch(option){
				case 1:
					System.out.println("--------- \nCrear Arreglo \nDar capacidad inicial del arreglo: ");
				    int capacidad = lector.nextInt();
				    modelo = new MVCModelo(capacidad); 
					System.out.println("Arreglo Dinamico creado");
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;

				case 2:
					System.out.println("Cargar Datos");
				try {
					System.out.println(modelo.cargarDatos());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("No fue posible cargar los datos");
					e.printStackTrace();
				}
					break;

				case 3:
					
					System.out.println("--------- \nBuscar por hora \nIngrese la hora a buscar:  ");
					System.out.println("Numero de viajes encontrados:  " + modelo.consultarViajesPorHora(lector.nextInt()).darTamano() + "\n---------");						
					break;
				case 4:
					
					System.out.println("--------- \nOrdenar los viajes buscados \nIngrese la hora a ordenar: ");
					String rta = modelo.shellSort(modelo.consultarViajesPorHora(lector.nextInt()));
					System.out.println(rta);
					System.out.println( "\n---------");						
					break;
				case 5: 
					System.out.println("--------- \nOrdenar los viajes buscados \nIngrese la hora a ordenar: ");
					String rta1 = modelo.mergeSort(modelo.consultarViajesPorHora(lector.nextInt()));
					System.out.println(rta1);
					System.out.println( "\n---------");						
					break;
				case 6: 
					System.out.println("--------- \nOrdenar los viajes buscados \nIngrese la hora a ordenar: ");
					String rta2 = modelo.quickSortStart(modelo.consultarViajesPorHora(lector.nextInt()));
					System.out.println(rta2);
					System.out.println( "\n---------");						
					break;
					
				case 7:
					System.out.println("--------- \nComparar los ordenamientos \nIngrese la hora para comparar ordenamientos: ");
					modelo.compararOrdenamientos(modelo.consultarViajesPorHora(lector.nextInt()));
					System.out.println( "\n---------");
					break;
				case 9: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
