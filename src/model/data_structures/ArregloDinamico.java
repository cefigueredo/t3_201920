package model.data_structures;

import java.util.Random;

import model.logic.UBERTrip;

/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico <T> implements IArregloDinamico< T>{
		/**
		 * Capacidad maxima del arreglo
		 */
        private int tamanoMax;
		/**
		 * Numero de elementos presentes en el arreglo (de forma compacta desde la posicion 0)
		 */
        private int tamanoAct;
        /**
         * Arreglo de elementos de tamaNo maximo
         */
        private T elementos[ ];

        /**
         * Construir un arreglo con la capacidad maxima inicial.
         * @param max Capacidad maxima inicial
         */
		public ArregloDinamico( int max )
        {
               elementos = (T[]) new UBERTrip[max];
               tamanoMax = max;
               tamanoAct = 0;
        }
        
		public void agregar( T dato )
        {
               if ( tamanoAct == tamanoMax )
               {  // caso de arreglo lleno (aumentar tamaNo)
                    tamanoMax = 2 * tamanoMax;
                    T[] copia = (T[])elementos;
                    elementos =  (T[]) new UBERTrip[tamanoMax];
                    for ( int i = 0; i < tamanoAct; i++)
                    {
                     	 elementos[i] = copia[i];
                    } 
            	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
               }	
               elementos[tamanoAct] = dato;
               tamanoAct++;
       }
		//M�todo agregar pero no imprime al aumentar el tama�o del arreglo.
		public void agregarEnBackground(T dato)
		 {
            if ( tamanoAct == tamanoMax )
            {  // caso de arreglo lleno (aumentar tamaNo)
                 tamanoMax = 2 * tamanoMax;
                 T[] copia = (T[])elementos;
                 elementos =  (T[]) new UBERTrip[tamanoMax];
                 for ( int i = 0; i < tamanoAct; i++)
                 {
                  	 elementos[i] = copia[i];
                 } 
         	   
            }	
            elementos[tamanoAct] = dato;
            tamanoAct++;
		 }

		public int darCapacidad() {
			return tamanoMax;
		}

		public int darTamano() {
			return tamanoAct;
		}

		public T darElemento(int i) {
			// TODO implementar
			
			return elementos[i];
		}

		public T buscar(T dato) {
			// TODO implementar
			// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
			for(T actual: elementos)
			{
				if(((Comparable<T>) actual).compareTo(dato)==0)
				{
					return actual;
				}
			}
			return null;
		}
		
		public T eliminar(T dato) {
			// TODO implementar
			// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
			T rta=null;
			for(int i=0;i<tamanoAct;i++)
			{
				if(((Comparable<T>) elementos[i]).compareTo(dato)==0)
				{
					rta=elementos[i];
					for(int j=i+1;j<tamanoAct;j++)
					{
						elementos[j-1]=elementos[j];
					}
					break;
				}
			}
			return rta;
		}

		@Override
		public int compareTo(T o) {
			// TODO Auto-generated method stub
			return 0;
		}
		public void exch(int pos1, int pos2)
		{
			T aux =elementos[pos1];
			elementos[pos1]=elementos[pos2];
			elementos[pos2]=aux;
		}

		/**
		 * Agrega elemento en posici�n dada.
		 */
		public void posicionarDato(T dato, int pos) {
			if(pos <= tamanoAct) {
				elementos[pos] = dato;
			}
		}

		public void shuffleArray(ArregloDinamico<UBERTrip> pViajes)
		{
		    UBERTrip temp;
		    int index;
		    Random random = new Random();
		    for (int i = pViajes.tamanoAct - 1; i > 0; i--)
		    {
		        index = random.nextInt(i + 1);
		        temp = pViajes.darElemento(index);
		        pViajes.posicionarDato(pViajes.darElemento(i), index); 
		        pViajes.posicionarDato(temp, i);;
		    }
		}
}
