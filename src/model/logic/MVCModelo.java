package model.logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import com.opencsv.CSVReader;

import controller.Controller;
import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;

/**
 * Definicion del modelo del mundo
 *
 */
public class  MVCModelo<T> {
	/**
	 * Atributos del modelo del mundo
	 */
	private ArregloDinamico<UBERTrip> datos;

	
	private ArregloDinamico<UBERTrip> viajesPorHora;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos = new ArregloDinamico <UBERTrip>(7);
	}
	
	/**
	 * Constructor del modelo del mundo con capacidad dada
	 * @param tamano
	 */
	public MVCModelo(int capacidad)
	{
		datos = new ArregloDinamico(capacidad);
	}
	
	public String cargarDatos() throws Exception{
		long tiempo=System.currentTimeMillis();
		if(datos==null)datos=new ArregloDinamico<UBERTrip>(1);
		String rta = "";
		CSVReader reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv"));
		String[] elementos = null;
		reader.readNext(); //Se salta la primera l�nea.
		while((elementos = reader.readNext()) != null ) {
			
			UBERTrip dato = new UBERTrip(Double.parseDouble(elementos[0]), Double.parseDouble(elementos[1]), Double.parseDouble(elementos[2]), Double.parseDouble(elementos[3]), Double.parseDouble(elementos[4]), Double.parseDouble(elementos[5]), Double.parseDouble(elementos[6]));
			datos.agregar(dato);
		}
		tiempo=System.currentTimeMillis()-tiempo;
		rta = "Viajes le�dos: "+datos.darTamano()+"\n Zona or�gen primer viaje: "+datos.darElemento(0).getSourceid()
				+" Zona destino primer viaje: "+datos.darElemento(0).getDstid()+" Hora: "+datos.darElemento(0).getHod()
				+" Tiempo promedio: "+datos.darElemento(0).getMeanTravelTime()+"\n Zona or�gen �ltimo viaje: "+datos.darElemento(datos.darTamano()-1).getSourceid()
				+" Zona destino �ltimo viaje: "+datos.darElemento(datos.darTamano()-1).getDstid()+" Hora: "+datos.darElemento(datos.darTamano()-1).getHod()
				+" Tiempo promedio: "+datos.darElemento(datos.darTamano()-1).getMeanTravelTime()+"\n Tiempo de carga:"+ tiempo+ "ms";
		return rta;
		
	}
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return datos.darTamano();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(UBERTrip dato)
	{	
		datos.agregar(dato);
	}
	
	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public UBERTrip buscar(UBERTrip dato)
	{
		return (UBERTrip) datos.buscar(dato);
	}
	
	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public UBERTrip eliminar(UBERTrip dato)
	{
		return (UBERTrip) datos.eliminar(dato);
	}
	
	public ArregloDinamico<UBERTrip> consultarViajesPorHora(int pHora) {
		int rta = 0;
		viajesPorHora = new ArregloDinamico<UBERTrip>(1);
		for(int i = 0; i < datos.darTamano(); i++) {
			if(datos.darElemento(i).getHod() == pHora) {
				viajesPorHora.agregarEnBackground(datos.darElemento(i));
				rta++;
			}
		}
		return viajesPorHora;
	}
	
	public String shellSort(ArregloDinamico<UBERTrip> pViajes) throws Exception{
		String rta ="";
		int tam = pViajes.darTamano();
		long tiempo = System.currentTimeMillis();
		int h = 1;
		while (h < tam/3)
			h = 3*h + 1;
		while (h >= 1) {
			for (int i = h; i < tam; i++) {
				for (int j = i; j >= h && pViajes.darElemento(j).compareTo(pViajes.darElemento(j-h)) < 0; j -= h) {
					pViajes.exch(j, j-h);
				}
			}
			h = h/3;
		}
		tiempo = System.currentTimeMillis() - tiempo;
		int m = 0;
		int n = pViajes.darTamano() - 1;
		
		rta +="Tiempo en milisegundos que tomo el ordenamiento shellsort: "+ tiempo + "\n Los 10 primeros viajes al ordenar son: \n";
		while(pViajes.darElemento(m) != null && m < 10 ) {
			rta = rta + pViajes.darElemento(m).getSourceid() + ", " + pViajes.darElemento(m).getDstid()
					+", "+pViajes.darElemento(m).getHod()+", "+ pViajes.darElemento(m).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(m).getStandardDeviationTravelTime()+", "+pViajes.darElemento(m).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(m).getGeometricStandardDeviationTravelTime()+ "\n";
			 m++;
		}
		rta += "\n Los 10 ultimos viajes al ordenar son: \n";
		while(pViajes.darElemento(n) != null && n > pViajes.darTamano() - 10  ) {
			rta = rta + pViajes.darElemento(n).getSourceid() + ", " + pViajes.darElemento(n).getDstid()
					+", "+pViajes.darElemento(n).getHod()+", "+ pViajes.darElemento(n).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(n).getStandardDeviationTravelTime()+", "+pViajes.darElemento(n).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(n).getGeometricStandardDeviationTravelTime()+ "\n";
			n--;
		}
		return rta;
	}
	
	public String mergeSort(ArregloDinamico<UBERTrip> pViajes) throws Exception {
		String rta = "";
		long tiempo = System.currentTimeMillis();
		ArregloDinamico<UBERTrip> aux = new ArregloDinamico<>(pViajes.darTamano());
		sort1(pViajes, aux, 0, pViajes.darTamano()- 1); 
		
		for(int i = 0; i < aux.darTamano(); i++) {
			pViajes.posicionarDato(aux.darElemento(i), i);
		}
		aux = null;
		tiempo = System.currentTimeMillis() - tiempo;
		int m = 0;
		int n = pViajes.darTamano() - 1;
		
		rta += "Tiempo en milisegundos que tomo el ordenamiento mergeSort: "+ tiempo + "\n Los 10 primeros viajes al ordenar son: \n";
		while(pViajes.darElemento(m) != null && m < 10 ) {
			rta = rta + pViajes.darElemento(m).getSourceid() + ", " + pViajes.darElemento(m).getDstid()
					+", "+pViajes.darElemento(m).getHod()+", "+ pViajes.darElemento(m).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(m).getStandardDeviationTravelTime()+", "+pViajes.darElemento(m).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(m).getGeometricStandardDeviationTravelTime()+ "\n";
			 m++;
		}
		rta += "\n Los 10 ultimos viajes al ordenar son: \n";
		while(pViajes.darElemento(n) != null && n > pViajes.darTamano() - 10  ) {
			rta = rta + pViajes.darElemento(n).getSourceid() + ", " + pViajes.darElemento(n).getDstid()
					+", "+pViajes.darElemento(n).getHod()+", "+ pViajes.darElemento(n).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(n).getStandardDeviationTravelTime()+", "+pViajes.darElemento(n).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(n).getGeometricStandardDeviationTravelTime()+ "\n";
			n--;
		}
		rta += "\n Holaa...";
		return rta;
	}
	public void sort1(ArregloDinamico<UBERTrip> pViajes, ArregloDinamico<UBERTrip> aux, int lo, int hi) {
		if (hi <= lo) return; // Sort a[lo..hi].
		int mid = lo + (hi - lo)/2;
		sort1(pViajes, aux, lo, mid);
		// Sort left half.
		sort1(pViajes, aux, mid+1, hi);
		// Sort right half.
		sort2(pViajes, aux, lo, mid, hi);
	}
	public void sort2(ArregloDinamico<UBERTrip> pViajes, ArregloDinamico<UBERTrip> aux, int lo, int mid, int hi) {
		int i, j, k;
		for (i=lo; i<=hi; i++) //copia ambas mitades en el array auxiliar
            aux.posicionarDato(pViajes.darElemento(i), i);
		i = lo;
		j = mid + 1;
		k=lo;
		while (i<=mid && j<=hi) {
			if(aux.darElemento(j) != null && aux.darElemento(i) != null) {
				if ( aux.darElemento(i).compareTo(aux.darElemento(j)) <= 0 && aux.darElemento(j) != null && aux.darElemento(i) != null) {
					pViajes.posicionarDato(aux.darElemento(i), k); i++; k++;
				}
			}
			else {
				pViajes.posicionarDato(aux.darElemento(j), k); j++; k++;
				while (i<=mid) { 
					pViajes.posicionarDato(aux.darElemento(i), k); i++; k++;
				}
			}
		}
	}
	
	public String quickSortStart(ArregloDinamico<UBERTrip> pViajes) throws Exception {
		String rta = "";
		sort(pViajes);
		long tiempo = System.currentTimeMillis();
		
		tiempo = System.currentTimeMillis() - tiempo;
		int m = 0;
		int n = pViajes.darTamano() - 1;
		
		rta += "Tiempo en milisegundos que tomo el ordenamiento mergeSort: "+ tiempo + "\n Los 10 primeros viajes al ordenar son: \n";
		while(pViajes.darElemento(m) != null && m < 10 ) {
			rta = rta + pViajes.darElemento(m).getSourceid() + ", " + pViajes.darElemento(m).getDstid()
					+", "+pViajes.darElemento(m).getHod()+", "+ pViajes.darElemento(m).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(m).getStandardDeviationTravelTime()+", "+pViajes.darElemento(m).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(m).getGeometricStandardDeviationTravelTime()+ "\n";
			 m++;
		}
		rta += "\n Los 10 ultimos viajes al ordenar son: \n";
		while(pViajes.darElemento(n) != null && n > pViajes.darTamano() - 10  ) {
			rta = rta + pViajes.darElemento(n).getSourceid() + ", " + pViajes.darElemento(n).getDstid()
					+", "+pViajes.darElemento(n).getHod()+", "+ pViajes.darElemento(n).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(n).getStandardDeviationTravelTime()+", "+pViajes.darElemento(n).getGeometricMeanTravelTime()
					+", "+pViajes.darElemento(n).getGeometricStandardDeviationTravelTime()+ "\n";
			n--;
		}
		return rta;
	}
	
	private static int partition(ArregloDinamico<UBERTrip> pViajes, int lo, int hi)
	{
		int i=lo;
		int j=hi+1;
		UBERTrip v=pViajes.darElemento(0);
		int aux=0;
		if(pViajes.darElemento(0).compareTo(pViajes.darElemento(1))<=0)
		{
			if(pViajes.darElemento(0).compareTo(pViajes.darElemento(2))>=0)
			{
				v=pViajes.darElemento(0);
				
			}
			else if(pViajes.darElemento(1).compareTo(pViajes.darElemento(2))<=0)
			{
				v=pViajes.darElemento(1);
				aux=1;
			}
			else
			{
				v=pViajes.darElemento(2);
				aux=2;
			}
		}
		else
		{
			if(pViajes.darElemento(1).compareTo(pViajes.darElemento(2))>=0)
			{
				v=pViajes.darElemento(1);
				aux=1;
			}
			else if(pViajes.darElemento(0).compareTo(pViajes.darElemento(2))<=0)
			{
				v=pViajes.darElemento(0);
			}
			
			else
			{
				v=pViajes.darElemento(2);
				aux=2;
			}
		}
		pViajes.exch(lo, aux);
		while(true)
		{
			while(pViajes.darElemento(++i).compareTo(v)<0)
				if(i==hi) 
					break;
			while(pViajes.darElemento(--j)!=null && pViajes.darElemento(--j).compareTo(v)>0)
				if(j==lo)
					break;
			if(i>=j)
				break;
			pViajes.exch(i, j);
		}
		pViajes.exch(lo, j);
		return j;
	}
	
	private static void sort(ArregloDinamico<UBERTrip> a)
	{
		
		a.shuffleArray(a);
		quickSort(a, 0, a.darTamano());
	}
	private static void quickSort(ArregloDinamico<UBERTrip> pViajes, int lo, int hi) {
		
		if(hi<=lo) return;
		int j=partition(pViajes, lo, hi);
		quickSort(pViajes,lo,j-1);
		quickSort(pViajes, j+1, hi);
		
	}
	
	public void compararOrdenamientos(ArregloDinamico<UBERTrip> pViajes) throws Exception {
		ArregloDinamico<UBERTrip> aux1 = new ArregloDinamico<UBERTrip>(pViajes.darTamano());
		ArregloDinamico<UBERTrip> aux2 = new ArregloDinamico<UBERTrip>(pViajes.darTamano());
		
		long startTime = System.currentTimeMillis(); // medici�n tiempo actual
		this.shellSort(pViajes); 
		long endTime = System.currentTimeMillis(); // medici�n tiempo actual
		long duration = endTime - startTime; // duracion de ejecucion del algoritmo
		System.out.println("Tiempo de ordenamiento shell sort: " + duration + " milisegundos");
		
		startTime = System.currentTimeMillis();
		this.mergeSort(aux1);
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("Tiempo de ordenamiento merge sort: " + duration + " milisegundos");
		
		startTime = System.currentTimeMillis();
		this.quickSortStart(aux2);
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
		System.out.println("Tiempo de ordenamiento quick sort: " + duration + " milisegundos");

		
	}


}
